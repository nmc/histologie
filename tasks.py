#!/usr/bin/env python3

from invoke import task
import os
import hashlib

def get_list_of_images(root_directory):
    images = []
    for root, dirs, files in os.walk(root_directory):
        for file in files:
            if file.endswith('.btf'):
                images.append((root, os.path.join(root, file), file))
    return images

@task(help={'root_directory': 'The name of the root direrctory to recursivly search for image files'})
def convert(c, root_directory):
    """
    convert all big tiff images in directory and subdirectories
    """

    image_list = get_list_of_images(root_directory)
    print('Start looking for images')
    print(image_list)
    for image in image_list:
        c.run('vips dzsave "{0}" "{1}/output-{3}-{2}" --depth onetile --vips-progress --suffix .jpeg'.format(image[1], image[0], hashlib.md5(image[1].encode('utf-8')).hexdigest()[:8], image[2]))
    print('schnups')
